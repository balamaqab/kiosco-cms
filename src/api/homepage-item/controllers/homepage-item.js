'use strict';

/**
 *  homepage-item controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::homepage-item.homepage-item');
