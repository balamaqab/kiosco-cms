'use strict';

/**
 * homepage-item router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::homepage-item.homepage-item');
