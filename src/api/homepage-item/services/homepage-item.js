'use strict';

/**
 * homepage-item service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::homepage-item.homepage-item');
