module.exports = ({ env }) => ({
  transformer: {
    enabled: true,
    config: {
      responseTransforms: {
        removeAttributesKey: true,
        removeDataKey: true,
      },
    },
  },
  // "rest-cache": {
  //   config: {
  //     provider: {
  //       name: "memory",
  //       options: {
  //         max: 32767,
  //         maxAge: 3600,
  //       },
  //     },
  //     strategy: {
  //       contentTypes: [
  //         // "api::category.category",
  //         // "api::keyword.keyword",
  //         // "api::article.article",
  //       ],
  //     },
  //   },
  // },
  "duplicate-button": true,
  slugify: {
    enabled: true,
    config: {
      contentTypes: {
        article: {
          field: "slug",
          references: "title",
        },
        category: {
          field: "slug",
          references: "name",
        },
        keyword: {
          field: "slug",
          references: "keyword",
        },
      },
    },
  },
});
