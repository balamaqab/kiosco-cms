##!/usr/bin/env bash

LOCAL_UPLOADS_PATH=${LOCAL_UPLOADS_PATH-public/uploads/}

rsync -avz --delete $DOMAIN:$UPLOADS_PATH $LOCAL_UPLOADS_PATH
